#
# makefile for `deheader'
#
VERS=$(shell sed <deheader -n -e '/version\s*=\s*"\(.*\)"/s//\1/p')

SOURCES = README COPYING NEWS deheader deheader.xml deheader.1 Makefile control deheader-logo.png

all: deheader.1

deheader.1: deheader.xml
	xmlto man deheader.xml

deheader.html: deheader.xml
	xmlto html-nochunks deheader.xml

clean:
	rm -f *~ *.1 *.html test/*.o test/*~ MANIFEST

check: regress

regress:
	cd test; make --quiet regress
makeregress:
	cd test; make --quiet makeregress

PYLINTOPTS = --rcfile=/dev/null --reports=n \
	--msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" \
	--dummy-variables-rgx='^_'
SUPPRESSIONS = --disable="C0103,C0111,C0301,C0302,C0323,C1001,R0903,R0912,R0913,R0914,R0915,W0110,W0141,W0611,W0621,E0611"
pylint:
	@pylint $(PYLINTOPTS) $(SUPPRESSIONS) deheader

version:
	@echo $(VERS)

deheader-$(VERS).tar.gz: $(SOURCES)
	@ls $(SOURCES) | sed s:^:deheader-$(VERS)/: >MANIFEST
	@(cd ..; ln -s deheader deheader-$(VERS))
	(cd ..; tar -czf deheader/deheader-$(VERS).tar.gz `cat deheader/MANIFEST`)
	@ls -l deheader-$(VERS).tar.gz
	@(cd ..; rm deheader-$(VERS))

dist: deheader-$(VERS).tar.gz

release: deheader-$(VERS).tar.gz deheader.html
	shipper version=$(VERS) | sh -e -x

refresh: deheader.html
	shipper -N -w version=$(VERS) | sh -e -x
